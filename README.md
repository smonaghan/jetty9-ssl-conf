#Jetty 9 SSL configuration

The following project captures the necessary configuration for a 2-way ssl [jetty-distribution-9.1.2.v20140210](http://eclipse.org/downloads/download.php?file=/jetty/stable-9/dist/jetty-distribution-9.1.2.v20140210.tar.gz&r=1)

The .gitignore excludes all files not involved in a working configuration so that a pristine jetty-distribution-9.1.2.v20140210 can be downloaded and this project can be expanded into that instance. 

The conf directory maintains a *myproj* directory where the jks, containing both the keystore and truststore (password changeit) is maintained. This directory also maintains the associated p12 (password is blank) for installation into the browser. All certificates were generated using the tools available at https://github.com/eawagner/cert-scripts.

The start.d/ssl.ini defines properties which reference the jks as well as other related properties. Upon startup the etc/jetty-ssl.xml resolves those properties resulting in a working configuration.

All passwords defined in ssl.ini were obfuscated using the jetty password utility per [Secure Password Obfuscation](http://www.eclipse.org/jetty/documentation/current/configuring-security-secure-passwords.html)

```
java -cp lib/jetty-util-$JETTY_VERSION.jar org.eclipse.jetty.util.security.Password changeit
```

Once all is configured deploy someapp.war to webapps and ...

```
java -jar start.jar
https://localhost:8443/someapp
```